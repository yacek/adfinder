import requests
import bs4
import selenium
import threading
import time
import queue
import urllib
import sys
import argparse

from selenium import webdriver
from selenium.webdriver.common.proxy import *

headers = {'User-agent': 
		   'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:56.0) Gecko/20100101 Firefox/56.0'}
''' 
dict like {'http': 'http://146.185.203.254:8085',
	           'https': 'https://146.185.203.254:8085',
	           'ffprox: '146.185.203.254:8085'},
'''
proxy = None 

class ad:
	def __init__(self,keyword):
		self.base_google_url = 'https://www.google.com'
		self.q = queue.Queue()
		self.lock = threading.Lock()
		self.keyword = urllib.parse.quote(keyword)
		self.result = set()
		self.yahoo_url = 'https://search.yahoo.com/search?p='
		self.google_url = 'https://www.google.by/search?q='
		self.google_pages = []
		self.retry_count = 0
		self.session = requests.Session()
		self.session.headers.update(headers)
		if proxy:
			self.browser_setup(proxy)
			self.session.proxies.update(proxy)
		else:
			self.browser_setup()

	def browser_setup(self,proxy=None):
		firefoxprofile = webdriver.FirefoxProfile()
		prox = None
		if proxy:
			prox = Proxy({
        		'proxyType': ProxyType.MANUAL,
        		'httpProxy': proxy['ffprox'],
        		'ftpProxy': proxy['ffprox'],
        		'sslProxy': proxy['ffprox'],
        		'noProxy':''})
			firefoxprofile.set_proxy(prox)
			firefoxprofile.update_preferences()
		options = webdriver.firefox.options.Options()
		options.add_argument('-headless')
		options.add_argument('-private')
		self.browser = webdriver.Firefox(firefox_profile=firefoxprofile,
										 proxy=prox,firefox_options=options)

	def yahoo_ads(self):
		url = ''.join([self.yahoo_url,self.keyword])
		req = self.session.get(url)
		html = bs4.BeautifulSoup(req.text,'lxml')
		ads = html.find('div',id='right')
		if ads:
			dom = ads.findAll('a',class_='ad-domain')
			for d in dom:
				self.lock.acquire()
				self.result.add(d.text)
				self.lock.release()

	def yahoo_work(self,q):
		while True:
			task = q.get()
			if task is None:
				break
			self.yahoo_ads()
			self.q.task_done()

	def yahoo_start(self):
		for i in range(1,10):
			t = threading.Thread(target=self.yahoo_work,args=(self.q,))
			t.start()
		for i in range(50):
			self.q.put(i)
		self.q.join()

		for i in range(1,10):
			self.q.put(None)


	def close(self):
		self.browser.close()

	def process_google_page(self,page):
		page = bs4.BeautifulSoup(page,'lxml')
		ads = page.findAll('cite', class_='_WGk')
		if ads:
			for i in ads:
				self.lock.acquire()
				print('google ad',i.text)
				self.result.add(i.text)
				self.lock.release()

	def google_ads(self):
		url = ''.join([self.google_url,self.keyword])
		#print(url)
		req = self.browser.get(url)
		page = self.browser.page_source
		self.google_pages.append(page)
		soup = bs4.BeautifulSoup(page,'lxml')
		page_div = soup.find('div',id='navcnt')
		try:
			pages = page_div.findAll('a',class_='fl')
		except AttributeError as e:
			self.retry_count += 1
			if self.retry_count > 3:
				print('Retry limit exceeded')
				return
			print('Google error, retrying')
			self.browser.close()
			self.browser_setup()
			self.google_ads()
			#print(e)
			return
		print('OK!')
		pages_urls = []
		for page in pages[1:]:
			pages_urls.append(''.join([self.base_google_url,page['href']]))

		for url in pages_urls:
			self.get_google_page(url)
			time.sleep(5)
		for page in self.google_pages:
			self.process_google_page(page)

	def get_google_page(self,url):
		req = self.browser.get(url)
		time.sleep(1)
		self.browser.execute_script(
					"window.scrollTo(0, document.body.scrollHeight);")
		self.google_pages.append(self.browser.page_source)

def parse_args():
	parser = argparse.ArgumentParser(description='Process some integers.')
	
def main(request=None):
	if len(sys.argv) > 1:
		request = ' '.join(sys.argv[1:])
	a = ad(request)
	a.yahoo_start()
	a.google_ads()
	print(a.result)
	with open('ads.txt','w+') as file:
		for res in a.result:
			file.write(res+'\n')
	a.close()
	print('done')

if __name__ == '__main__':
	print('Ads scrapping module')
	main()
